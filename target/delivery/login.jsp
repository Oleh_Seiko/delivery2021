<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">

    <title>Login</title>
    </head>

<body>
    <a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
    <a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>
<h1>
    <fmt:message key="label.pleaseToLogin" />
</h1>

<form action="${pageContext.request.contextPath}/login" method="post">
    <%--Користувач<input type="radio">--%>
    <%--Адмін<input type="radio">--%>
    <p><fmt:message key="label.username" /> : <input type="text" name="Login" ></p>
    <p><fmt:message key="label.password" /> : <input type="password" name="Password"></p>
    <p><button><fmt:message key="label.login" /></button></p>

    <%--<p><input type="submit" value="Вхід"></p>--%>
</form>

<p><a href="${pageContext.request.contextPath}/index.jsp"><button><fmt:message key="label.home" /> </button></a></p>


</body>
</html>
