<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>Hello</title>
</head>

<body>
<a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
<a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>


 <h1>
     <fmt:message key="label.select" />
 </h1>

 <h2>
     <fmt:message key="label.choose" />
 </h2>

 <p><a href = "${pageContext.request.contextPath}/calculate"> <fmt:message key="label.calculate" /> </a></p>
 <p><a href = "login.jsp"> <fmt:message key="label.login" /></a></p>
 <p><a href = "registration.jsp"> <fmt:message key="label.register" /> </a></p>

 </body>

</html>
