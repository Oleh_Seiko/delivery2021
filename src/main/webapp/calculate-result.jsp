<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">

    <title>Calculation</title>
</head>
<body>
<a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
<a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>
<h1>
Calculate Result
</h1>
<p>City Start : ${city}</p>
<p>City Finish : ${city2}</p>
<p>Weight : ${weight}</p>
<p>Volume : ${volume}</p>

<p> <label><fmt:message key="label.distance" /> : </label> <b>${km}</b> <fmt:message key="label.km" />.</p>

<label><fmt:message key="label.sum" /> : </label><b>${sum}</b> <fmt:message key="label.gr" />.
</p>

<a href="${pageContext.request.contextPath}/calculate"><button>Calculation</button></a>
<a href="${pageContext.request.contextPath}/index.jsp"><button>Home</button></a>

</body>
</html>
