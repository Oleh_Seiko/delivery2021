<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">

    <title>Calculation</title>
</head>
<body>
    <a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
    <a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>


<legend>
    <h1>
        <fmt:message key="label.calculation" />
    </h1>
</legend>

<form action="${pageContext.request.contextPath}/calculate/result" method="post">
    <fieldset>
        <h3><fmt:message key="label.calculationOfTheCost" /></h3><br>

        <p><label><fmt:message key="label.cityStart" /> : </label>
            <select name="city">
                <c:forEach var="city" items="${cities}">
                    <option>${city}</option>
                </c:forEach>
            </select>
            <label><fmt:message key="label.cityFinish" /> : </label>
            <select name="city2">
                <c:forEach var="city2" items="${cities}">
                    <option>${city2}</option>
                </c:forEach>
            </select>
<%--            <label><fmt:message key="label.distance" /> : </label> <b>${distance}</b> <fmt:message key="label.km" />.</p>--%>
        <p>
            <label><fmt:message key="label.weight" /> : <input type="number" name="weight"></label>
            <label><fmt:message key="label.volume" /> : <input type="number" name="volume"></label>
<%--            <label><fmt:message key="label.sum" /> : </label><b>${sum}</b> <fmt:message key="label.gr" />.--%>
        </p>


    <p><button><fmt:message key="label.calculate" /></button></p>

    </fieldset>



</form>

    <p><a href="${pageContext.request.contextPath}/index.jsp"><button><fmt:message key="label.home" /> </button></a></p>

</body>
</html>
