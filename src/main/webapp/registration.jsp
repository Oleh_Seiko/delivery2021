<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
</head>
<body>
    <a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
    <a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>

<fieldset>

    <legend><h1><fmt:message key="label.register"/></h1></legend>

<form action="${pageContext.request.contextPath}/registration" method="post">
    <p><label><fmt:message key="label.name" />    : </label><input type="text" name="firstName"></p>
    <p><label><fmt:message key="label.surname" /> : </label><input type="text" name="lastName"></p>
    <p><label><fmt:message key="label.email" />   : </label><input type="email" name="username"></p>
    <p><label><fmt:message key="label.password" />: </label><input type="password" name="password"></p>

    <p><button><fmt:message key="label.register" /></button></p>

</form>
</fieldset>

        <p><a href="${pageContext.request.contextPath}/index.jsp"><button><fmt:message key="label.home" /> </button></a></p>

</body>
</html>
