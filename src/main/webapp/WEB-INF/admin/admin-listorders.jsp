<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>ListOrders</title>
</head>
<body>
Admin Orders
<table>
    <thead>
    <tr>
        <th>OrderId</th>
        <th>UserId</th>
        <th>DateStart</th>
        <th>CityStart</th>
        <th>CityFinish</th>
        <th>Weight</th>
        <th>Volume</th>
        <th>Km</th>
        <th>Sum</th>
        <th>Status</th>
        <th>Change Status</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="order" items="${allOrders}">
        <tr>
            <td><c:out value="${order.id}" /></td>
            <td><c:out value="${order.user.id}" /></td>
            <td><c:out value="${order.dateStart}" /></td>
            <td><c:out value="${order.cityStart}" /></td>
            <td><c:out value="${order.cityFinish}" /></td>
            <td><c:out value="${order.weight}" /></td>
            <td><c:out value="${order.volume}" /></td>
            <td><c:out value="${order.km}" /></td>
            <td><c:out value="${order.sum}" /></td>
            <td><c:out value="${order.status}" /></td>

            <td><form action="${pageContext.request.contextPath}/admin/orders/one-order" method="post">
                <input hidden name="oneOrder" value="${order.id}">
            <button>Open</button></form></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
    <a href="${pageContext.request.contextPath}/admin"><button>Home</button></a>

</body>
</html>
