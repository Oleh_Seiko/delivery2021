<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>
Admin One Order
</h1>
<table>
    <thead>
    <tr>
        <th>OrderId</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>DateStart</th>
        <th>CityStart</th>
        <th>CityFinish</th>
        <th>Weight</th>
        <th>Volume</th>
        <th>Km</th>
        <th>Sum</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    User: <tr>
        <td>${order.id}</td><td>${order.user.firstName}</td> <td>${order.user.lastName}</td> <td>${order.dateStart}</td> <td>${order.cityStart}</td>
        <td>${order.cityFinish}</td> <td>${order.weight}</td> <td>${order.volume}</td> <td>${order.km}</td> <td>${order.sum}</td>
        <td>${order.status}</td>
    </tr>
    </tbody>
</table>

        <form action="${pageContext.request.contextPath}/admin/order/change-status" methods="post">
            <input hidden name="order-id" value="${order.id}">

            <p><select name="status">
                <c:forEach var="status" items="${statuses}">
                    <option>${status}</option>
                </c:forEach>
            </select>
                <button>Change Status</button></p>
        </form>

</body>
    <p><a href="${pageContext.request.contextPath}/admin/orders"><button>All orders</button></a></p>
    <a href="${pageContext.request.contextPath}/admin"><button>Home</button></a>
</html>
