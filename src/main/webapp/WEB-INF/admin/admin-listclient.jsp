<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>ListClient</title>
</head>
<body>
Admin-ListClient

<table>
    <thead>
    <tr>
        <th>id</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Username</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="allUser" items="${allUsers}">
        <tr>
            <td><c:out value="${allUser.id}" /></td>
            <td><c:out value="${allUser.firstName}" /></td>
            <td><c:out value="${allUser.lastName}" /></td>
            <td><c:out value="${allUser.username}" /></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<p>
    <a href="${pageContext.request.contextPath}/admin">Home</a>
</p>
</body>
</html>
