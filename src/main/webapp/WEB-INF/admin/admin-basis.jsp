<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>Client</title>
</head>
<body>
    <a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
    <a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>
<h1>ADMIN PAGE</h1>
    ADMIN :
    <p>
        <a href="${pageContext.request.contextPath}/admin/client">Client</a>
    </p>
    <p>
        <a href="${pageContext.request.contextPath}/admin/orders">Orders</a>
    </p>
<p><a href="${pageContext.request.contextPath}/index.jsp"><button><fmt:message key="label.home" /> </button></a></p>

</body>
</html>
