<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>Client</title>
</head>
<body>
client.jsp
    <a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
    <a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>
<h1>
    <fmt:message key="label.user" />: ${user.firstName} ${user.lastName}
</h1>


    <form action="${pageContext.request.contextPath}/client/result" method="post">
        <fieldset>
            <lagend><h3><fmt:message key="label.calculationOfTheCost" /> </h3></lagend><br>
            <p><fmt:message key="label.dateStart" /> : <input type="date" name="date"></p>
            <p><label><fmt:message key="label.cityStart" /> : </label>
                <select name="city">
                    <c:forEach var="city" items="${cities}">
                        <option>${city}</option>
                    </c:forEach>
                </select>
                <label><fmt:message key="label.cityFinish" /> : </label>
                <select name="city2">
                    <c:forEach var="city2" items="${cities}">
                        <option>${city2}</option>
                    </c:forEach>
                </select>
<%--                <label><fmt:message key="label.distance" /> : </label> <b><input type="number" name="distance"> : ${km}</b> <fmt:message key="label.km" />.</p>--%>
            <p>
                <label><fmt:message key="label.weight" /> : <input type="number" name="weight"></label>
                <label><fmt:message key="label.volume" /> : <input type="number" name="volume"></label>
<%--                <label><fmt:message key="label.sum" /> : </label><b><input type="number" name="sum" value="${sum}"></b> <fmt:message key="label.gr" />.--%>
            </p>
<%--            <label><fmt:message key="label.status" /> : </label>--%>
<%--            <select name="status">--%>
<%--                <c:forEach var="status" items="${statuses}">--%>
<%--                    <option>${status}</option>--%>
<%--                </c:forEach>--%>
<%--            </select>--%>

        </fieldset>

        <p><button><fmt:message key="label.calculate" /></button> </p>
<%--    <button><fmt:message key="label.makeAnApplication" /></button>--%>

    </form>

        <a href="${pageContext.request.contextPath}/client/order"><button><fmt:message key="label.listOfApplications" /> </button></a>
    <p><a href="${pageContext.request.contextPath}/index.jsp"><button><fmt:message key="label.home" /> </button></a></p>
</body>
</html>
