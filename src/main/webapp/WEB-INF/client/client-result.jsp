<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta charset="UTF-8">
    <title>Client-result</title>
</head>
<body>
<a href="?sessionLocale=en"><button><fmt:message key="label.lang.en" /></button></a>
<a href="?sessionLocale=ua"><button><fmt:message key="label.lang.ua" /></button></a>
<h1>
    <fmt:message key="label.user" />: ${user.firstName} ${user.lastName}
    Client Result
</h1>


<form action="${pageContext.request.contextPath}/client/order" method="post">
    <fieldset>
        <lagend><h3><fmt:message key="label.calculationOfTheCost" /> </h3></lagend><br>
        <p>Date Start : ${date}</p>
        <p>City Start : ${city}</p>
        <p>City Finish : ${city2}</p>
        <p>Weight : ${weight}</p>
        <p>Volume : ${volume}</p>

        <p> <label><fmt:message key="label.distance" /> : </label> <b> ${km}</b> <fmt:message key="label.km" />.</p>

            <label><fmt:message key="label.sum" /> : </label><b> ${sum}</b> <fmt:message key="label.gr" />.
        </p>

<%--        <label><fmt:message key="label.status" /> : </label>--%>
<%--        <select name="status">--%>
<%--            <c:forEach var="status" items="${statuses}">--%>
<%--                <option>${status}</option>--%>
<%--            </c:forEach>--%>
<%--        </select>--%>

    </fieldset>
        <button><fmt:message key="label.makeAnApplication" /></button></p>

</form>

<p><a href="${pageContext.request.contextPath}/index.jsp"><button><fmt:message key="label.home" /> </button></a></p>
</body>
</html>

