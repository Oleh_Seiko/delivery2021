<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title>Client Order</title>
</head>
<body>
Client Order<br>
<fmt:message key="label.user" />: ${user.firstName} ${user.lastName}

<h1>
    <table>
        <thead>
        <tr>
            <th>dateStart</th>
            <th>cityStart</th>
            <th>cityFinish</th>
            <th>weight</th>
            <th>volume</th>
            <th>km</th>
            <th>sum</th>
            <th>status</th>
        </tr>
        </thead>
        <tbody>
    <c:forEach var="order" items="${orders}">
        <form action="${pageContext.request.contextPath}/client/order/pay" method="post">
        <tr>
            <td><c:out value="${order.dateStart}" /></td>
            <td><c:out value="${order.cityStart}" /></td>
            <td><c:out value="${order.cityFinish}" /></td>
            <td><c:out value="${order.weight}" /></td>
            <td><c:out value="${order.volume}" /></td>
            <td><c:out value="${order.km}" /></td>
            <td><c:out value="${order.sum}" /></td>
            <td><c:out value="${order.status}" /></td>
            <td><c:if test="${order.status == 'WAITING_FOR_PAY'}">
                <input hidden name="order-id" value="${order.id}">
                <button>PAY</button>
            </c:if></td>
        </tr>
        </form>
    </c:forEach>
        </tbody>
    </table>
</h1>

<a href="${pageContext.request.contextPath}/client-result"><button>Back</button></a>
<a href="${pageContext.request.contextPath}/client"><button>Home</button></a>
</body>
</html>
