package ua.delivery.model.service;

import ua.delivery.model.dao.UserDao;
import ua.delivery.model.dao.impl.UserDaoImpl;
import ua.delivery.model.entity.Distance;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Status;

import java.util.List;

public class UserService {

    private UserDao userDao;

    public UserService() {
        userDao = new UserDaoImpl();
    }

    public void create(User user) {
        userDao.create(user);

    }

    public User findByUsernameAndPassword(String username, String password) {
        return userDao.findByUsernameAndPassword(username, password);
    }

    public Distance findDistanceByCities(String cityStart, String cityFinish) {
        return userDao.findDistanceByCities(cityStart, cityFinish);
    }

    public void createOrder(Order order) {
        userDao.createOrder(order);
    }

    public List<Order> findByOrderUserId(int id) {
        return userDao.findOrdersByUserId(id);
    }

    public List<User> findAllUsers() {
        return userDao.findAllUsers();
    }

    public List<Order> findAllOrders() {
        return userDao.findAllOrders();
    }

    public User findUserById(int id) {
        return userDao.findUserById(id);

    }

    public Order findOrderById(int id) {
        return userDao.findOrderById(id);
    }

    public void changeStatus(int id, Status status) {
        userDao.changeStatus(id,status);
    }
    public void deleteOrder(int id){
        userDao.deleteOrder(id);
    }
}
