package ua.delivery.model.entity.enums;

public enum Status {
    CREATED,
    IN_PROCESSING,
    WAITING_FOR_PAY,
    PAID,
    SENT,
    DELIVERED,
    REJECTED
}
