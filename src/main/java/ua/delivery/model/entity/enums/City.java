package ua.delivery.model.entity.enums;

public enum City {
    LVIV,
    KYIV,
    KHARKIV,
    DNIPRO,
    VINNITSA
}
