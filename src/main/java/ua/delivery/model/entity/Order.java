package ua.delivery.model.entity;

import ua.delivery.model.entity.enums.Status;

import java.sql.Date;
import java.util.Objects;

public class Order {
    private int id;
    private User user;
    private Date dateStart;
    private String cityStart;
    private String cityFinish;
    private int weight;
    private int volume;
    private int km;
    private int sum;
    private Status status;

    public Order() {
    }

    public Order(int id, User user, Date dateStart, String cityStart, String cityFinish, int weight, int volume, int km, int sum, Status status) {
        this.id = id;
        this.user = user;
        this.dateStart = dateStart;
        this.cityStart = cityStart;
        this.cityFinish = cityFinish;
        this.weight = weight;
        this.volume = volume;
        this.km = km;
        this.sum = sum;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getCityStart() {
        return cityStart;
    }

    public void setCityStart(String cityStart) {
        this.cityStart = cityStart;
    }

    public String getCityFinish() {
        return cityFinish;
    }

    public void setCityFinish(String cityFinish) {
        this.cityFinish = cityFinish;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && weight == order.weight && volume == order.volume && km == order.km && sum == order.sum && user.equals(order.user) && dateStart.equals(order.dateStart) && cityStart.equals(order.cityStart) && cityFinish.equals(order.cityFinish) && status == order.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, dateStart, cityStart, cityFinish, weight, volume, km, sum, status);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", user=" + user +
                ", dateStart=" + dateStart +
                ", cityStart='" + cityStart + '\'' +
                ", cityFinish='" + cityFinish + '\'' +
                ", weight=" + weight +
                ", volume=" + volume +
                ", km=" + km +
                ", sum=" + sum +
                ", status=" + status +
                '}';
    }
}
