package ua.delivery.model.entity;

import java.util.Objects;

public class Distance {
    private String cityStart;
    private String cityFinish;
    private int km;

    public Distance() {
    }

    public Distance(String cityStart, String cityFinish, int km) {
        this.cityStart = cityStart;
        this.cityFinish = cityFinish;
        this.km = km;
    }

    public String getCityStart() {
        return cityStart;
    }

    public void setCityStart(String cityStart) {
        this.cityStart = cityStart;
    }

    public String getCityFinish() {
        return cityFinish;
    }

    public void setCityFinish(String cityFinish) {
        this.cityFinish = cityFinish;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distance distance = (Distance) o;
        return km == distance.km && cityStart.equals(distance.cityStart) && cityFinish.equals(distance.cityFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityStart, cityFinish, km);
    }

    @Override
    public String toString() {
        return "Distance{" +
                "cityStart='" + cityStart + '\'' +
                ", cityFinish='" + cityFinish + '\'' +
                ", km=" + km +
                '}';
    }
}
