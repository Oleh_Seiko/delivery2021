package ua.delivery.model.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.model.dao.UserDao;
import ua.delivery.model.entity.Distance;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Role;
import ua.delivery.model.entity.enums.Status;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private static final Logger logger = LogManager.getLogger(UserDaoImpl.class);


    private static final String CREATE_USER = "INSERT INTO user" + "(firstName, lastName, username, password, role)" +
            "VALUES(?, ?, ?, ?, ?)";
    private static final String SELECT_FIND_BY_USERNAME_AND_PASSWORD = "SELECT * FROM user WHERE username = ? AND password = ?";
    private static final String SELECT_FIND_BY_CITY_AND_DISTANCE = "SELECT * FROM distance WHERE downloadcity = ? AND unloadcity = ?";
    private static final String CREATE_ORDER = "INSERT INTO orders" + "(id, userId, dateStart, cityStart, cityFinish, weight, volume, sum, km, status)"
            + "VALUES(?,?,?,?,?,?,?,?,?,?)";
    private static final String SELECT_FIND_BY_USER_ID = "SELECT * FROM orders WHERE userId = ?";
    private static final String SELECT_FIND_ALL_USERS = "SELECT * FROM user";
    private static final String SELECT_FIND_ALL_ORDERS = "SELECT * FROM orders";
    private static final String SELECT_FIND_ORDER_BY_ID = "SELECT * FROM orders WHERE id = ?";
    private static final String SELECT_FIND_USER_BY_ID = "SELECT * FROM user WHERE id = ?";
    private static final String UPDATE_STATUS_SQL = "UPDATE orders SET status = ? WHERE id = ?";
    private static final String DELETE_ORDER_BY_ID = "DELETE FROM orders WHERE id = ?";

    private Connection connection;

    String url = "jdbc:mysql://localhost:3306/delivery?useUnicode=true&serverTimezone=UTC";
    String username = "root";
    String password = "root";

    public UserDaoImpl() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void create(User user) {
        logger.info("UserDao create");

        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER)) {
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getUsername());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getRole().name());
            preparedStatement.executeUpdate();

            logger.info("User saved");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) {
        logger.info("FindByUsernameAndPassword create");

        try (PreparedStatement pass = connection.prepareStatement(SELECT_FIND_BY_USERNAME_AND_PASSWORD)) {
            pass.setString(1, username);
            pass.setString(2, password);

            ResultSet resultSet = pass.executeQuery();

            User user = new User();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String username1 = resultSet.getString("username");
                String password1 = resultSet.getString("password");
                String role = resultSet.getString("role");

                logger.info("Role " + role);

                user.setId(id);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setUsername(username1);
                user.setPassword(password1);
                user.setRole(Role.valueOf(role));

                logger.info("username " + "" + username + "" + "password " + "" + password);
            }
            logger.info("USER FROM DB USER RESULT" + "" + user);

            return user;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Distance findDistanceByCities(String cityStart, String cityFinish) {

        logger.info("findByCitiesAndDistance create()");

        try (PreparedStatement distan = connection.prepareStatement(SELECT_FIND_BY_CITY_AND_DISTANCE)) {
            distan.setString(1, cityStart);
            distan.setString(2, cityFinish);

            ResultSet resultSet = distan.executeQuery();

            Distance distances = new Distance();

            while (resultSet.next()) {
                distances.setCityStart(resultSet.getString("downloadcity"));
                distances.setCityFinish(resultSet.getString("unloadcity"));
                distances.setKm(resultSet.getInt("distance"));
            }
            logger.info("distance ResultSET " + distances);
            return distances;

        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("NOT found distance");
        }
        return null;
    }

    @Override
    public void createOrder(Order order) {
        logger.info("created createOrder()");

        try (PreparedStatement psCreateUser = connection.prepareStatement(CREATE_ORDER)) {
            psCreateUser.setInt(1, order.getId());
            psCreateUser.setInt(2, order.getUser().getId());
            psCreateUser.setDate(3, order.getDateStart());
            psCreateUser.setString(4, order.getCityStart());
            psCreateUser.setString(5, order.getCityFinish());
            psCreateUser.setInt(6, order.getWeight());
            psCreateUser.setInt(7, order.getVolume());
            psCreateUser.setInt(8, order.getSum());
            psCreateUser.setInt(9, order.getKm());
            psCreateUser.setString(10, order.getStatus().name());
            psCreateUser.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("Don't createOrder");
        }
    }

    @Override
    public List<Order> findOrdersByUserId(int userId) {
        logger.info("created findOrdersByUserId");
        try (PreparedStatement stOrderUserId = connection.prepareStatement(SELECT_FIND_BY_USER_ID)) {
            stOrderUserId.setInt(1, userId);
            ResultSet resultSet = stOrderUserId.executeQuery();
            List<Order> orders = new ArrayList<>();
            while (resultSet.next()) {
                Order order = new Order();
                int orderUserId = resultSet.getInt("userId");
                System.out.println("userId " + orderUserId);
                order.setId(resultSet.getInt("id"));
                order.setDateStart(resultSet.getDate("dateStart"));
                order.setCityStart(resultSet.getString("cityStart"));
                order.setCityFinish(resultSet.getString("cityFinish"));
                order.setWeight(resultSet.getInt("weight"));
                order.setVolume(resultSet.getInt("volume"));
                order.setKm(resultSet.getInt("km"));
                order.setSum(resultSet.getInt("sum"));
                order.setStatus(Status.valueOf(resultSet.getString("status")));

                orders.add(order);

                logger.info("OrderUserId " + orders);
            }
            return orders;

        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("NOT found Order userId");
            return null;
        }
    }

    @Override
    public List<User> findAllUsers() {
        logger.info("created method findAllUsers");
        try (PreparedStatement psFindAllUsers = connection.prepareStatement(SELECT_FIND_ALL_USERS)) {
            ResultSet resultSet = psFindAllUsers.executeQuery();
            List<User> allUsers = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstname"));
                user.setLastName(resultSet.getString("lastname"));
                user.setUsername(resultSet.getString("username"));
//                user.setRole(Role.valueOf(resultSet.getString("role")));

                allUsers.add(user);
                logger.info("allUsers " + allUsers);

            }
            return allUsers;

        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("NOT found Users");
            return null;
        }
    }

    @Override
    public List<Order> findAllOrders() {
        logger.info("created findAllOrders");
        try (PreparedStatement psFindAllOrders = connection.prepareStatement(SELECT_FIND_ALL_ORDERS)) {
            ResultSet resultSet = psFindAllOrders.executeQuery();
            List<Order> orders = new ArrayList<>();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("id"));
                order.setDateStart(resultSet.getDate("dateStart"));
                order.setCityStart(resultSet.getString("cityStart"));
                order.setCityFinish(resultSet.getString("cityFinish"));
                order.setWeight(resultSet.getInt("weight"));
                order.setVolume(resultSet.getInt("volume"));
                order.setKm(resultSet.getInt("km"));
                order.setSum(resultSet.getInt("sum"));
                order.setStatus(Status.valueOf(resultSet.getString("status")));
                final int userId = resultSet.getInt("userId");
                order.setUser(new User(userId));

                orders.add(order);
                logger.info("orders " + orders);

                System.out.println("userId " + userId);
            }
            return orders;
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("NOT found Orders");
            return null;
        }
    }

    @Override
    public Order findOrderById(int id) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_FIND_ORDER_BY_ID);) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            final Order order = new Order();
            while (resultSet.next()) {
                order.setId(resultSet.getInt("id"));
                order.setDateStart(resultSet.getDate("dateStart"));
                order.setCityStart(resultSet.getString("cityStart"));
                order.setCityFinish(resultSet.getString("cityFinish"));
                order.setWeight(resultSet.getInt("weight"));
                order.setVolume(resultSet.getInt("volume"));
                order.setKm(resultSet.getInt("km"));
                order.setSum(resultSet.getInt("sum"));
                order.setStatus(Status.valueOf(resultSet.getString("status")));
                final int userId = resultSet.getInt("userId");
                order.setUser(new User(userId));

            }
                return order;

        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("NOT found OrderById");
            return null;
        }
    }

    @Override
    public void changeStatus(int id, Status status) {
        logger.info("changeStatus ");
        try(final PreparedStatement psChangeStatus = connection.prepareStatement(UPDATE_STATUS_SQL);){
            psChangeStatus.clearParameters();
            psChangeStatus.setString(1,status.name());
            psChangeStatus.setInt(2, id);
            final int update = psChangeStatus.executeUpdate();
            logger.info("UPDATED " + update);
        }catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("Don't changeStatus");

        }
    }

    @Override
    public void deleteOrder(int id) {
        try (final PreparedStatement psDeleteOrder = connection.prepareStatement(DELETE_ORDER_BY_ID)) {
            psDeleteOrder.setInt(1, id);
            psDeleteOrder.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("Don't delete ID");

        }
    }


    @Override
    public User findUserById(int id) {
        try (final PreparedStatement prFindUSerById = connection.prepareStatement(SELECT_FIND_USER_BY_ID)) {
            prFindUSerById.setInt(1, id);
            final ResultSet resultSet = prFindUSerById.executeQuery();
                User user = new User();
            while (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstName"));
                user.setLastName(resultSet.getString("lastName"));
                System.out.println("user " + user);
                logger.info("users " + user);
            }

            return user;
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.info("NOT found findUserById");
            return null;

        }
    }
}
