package ua.delivery.model.dao;

import ua.delivery.model.entity.Distance;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Status;

import java.util.List;

public interface UserDao {

    void create(User user);

    User findByUsernameAndPassword(String username, String password);

    Distance findDistanceByCities(String cityStart, String cityFinish);

    void createOrder(Order order);

    List<Order> findOrdersByUserId(int id);

    List<User> findAllUsers();

    List<Order> findAllOrders();

    Order findOrderById(int id);

    void changeStatus(int id, Status status);

    void deleteOrder(int id);

    User findUserById(int id);

}
