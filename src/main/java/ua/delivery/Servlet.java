package ua.delivery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.controller.command.*;
import ua.delivery.model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Servlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(Servlet.class);

    private Map<String, Command> commands = new HashMap<>();


    @Override
    public void init() {
        logger.info("created init()");

        commands.put("login", new Login(new UserService()));
        commands.put("registration", new Register(new UserService()));
        commands.put("delivery", new Delivery());
        commands.put("client", new Client(new UserService()));
        commands.put("admin", new Admin());
        commands.put("calculate", new Calculate(new UserService()));
        commands.put("calculate/result", new CalculateResult(new UserService()));
        commands.put("client/result", new ClientResult(new UserService()));
        commands.put("client/order", new ClientOrder(new UserService()));
        commands.put("client/order/pay", new ClientOrderPay(new UserService()));
        commands.put("admin/client", new AdminListClient(new UserService()));
        commands.put("admin/orders", new AdminListOrders(new UserService()));
        commands.put("admin/orders/one-order", new AdminOneOrder(new UserService()));
        commands.put("admin/order/change-status", new AdminOrderChangeStatus(new UserService()));
        commands.put("admin/order/delete", new AdminOrderDelete(new UserService()));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String path = request.getRequestURI();
        logger.info("RequestURI - " + path);
        path = path.replaceAll(".*/delivery/", "");
        logger.info("Path " + path);
        Command command = commands.getOrDefault(path,
                (r) -> "index.jsp");
        String page = command.execute(request);
        logger.info("Page " + page);
        if (page.contains("redirect:")) {
            response.sendRedirect(page.replace("redirect:", "/delivery"));
        } else {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
