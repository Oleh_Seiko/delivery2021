package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Role;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Login implements Command {
    private static final Logger logger = LogManager.getLogger(Login.class);

    private UserService userService;

    public Login(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created, execute()");
        String name = request.getParameter("Login");
        String password = request.getParameter("Password");

        logger.info("~~~" + name + " " + "~~~" + password);


        if (name == null || name.equals("") || password == null || password.equals("")){

            return "/login.jsp";
        }

        User user = userService.findByUsernameAndPassword(name, password);

        HttpSession session = request.getSession();
        session.setAttribute("user", user);

        logger.info("LOGIN " + user);

        if(user.getRole().equals(Role.ROLE_CLIENT)) {
            return "redirect:/client";
        }else if (user.getRole().equals(Role.ROLE_ADMIN)){
            return "redirect:/admin";
        }else {

            // TODO go to Service
            return "/login.jsp";
        }




    }

}
