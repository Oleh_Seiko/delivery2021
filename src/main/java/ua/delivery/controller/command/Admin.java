package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;

import javax.servlet.http.HttpServletRequest;

public class Admin implements Command {
    private static final Logger logger = LogManager.getLogger(Admin.class);

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("Created execute()");

        return "WEB-INF/admin/admin-basis.jsp";
    }
}
