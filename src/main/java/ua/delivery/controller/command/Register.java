package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Role;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class Register implements Command {
    private static final Logger logger = LogManager.getLogger(Register.class);

    private UserService userService;

    public Register(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String username = request.getParameter("username");
        String password = request.getParameter("password");


        if (firstName == null || firstName.equals("") ||
                lastName == null || lastName.equals("") ||
                username == null || username.equals("") ||
                password == null || password.equals("")) {

            return "/registration.jsp";
        } else {

            User user = new User();
            logger.info("user " + user);

            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setUsername(username);
            user.setPassword(password);
            user.setRole(Role.ROLE_CLIENT);

            logger.info("Register " + user);

            userService.create(user);

            return "redirect:/login.jsp";
        }
    }
}
