package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.enums.Status;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class ClientOrderPay implements Command {
    private static final Logger logger = LogManager.getLogger(ClientOrderPay.class);
    public UserService userService;

    public ClientOrderPay(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");
        final String orderId = request.getParameter("order-id");
        final int orderIdInt = Integer.parseInt(orderId);
        System.out.println("OrderIDInt " +orderIdInt);

        userService.changeStatus(orderIdInt, Status.PAID);
        return "redirect:/client/order";
    }
}
