package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Distance;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.City;
import ua.delivery.model.entity.enums.Status;
import ua.delivery.model.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

public class Client implements Command {
    private static final Logger logger = LogManager.getLogger(Client.class);

    private UserService userService;


    public Client(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");

//        request.setAttribute("nameofuser", "Vasya");

        City[] cities = City.values();
        request.setAttribute("cities", cities);
        Status[] statuses = Status.values();
        request.setAttribute("statuses", statuses);

//        String uploadDate = request.getParameter("date");
//        logger.info("date " + uploadDate);
//
//        String downloadcity = request.getParameter("city");
//        logger.info("downloadcity " + downloadcity);
//
//        String unloadcity = request.getParameter("city2");
//        logger.info("unloadcity " + unloadcity);
//
//        String weight = request.getParameter("weight");
//        logger.info("weight " + weight);
//
//        String volume = request.getParameter("volume");
//        logger.info("volume " + volume);

//        String sum = request.getParameter("sum");
//        logger.info("sum " + sum);
//
//        String km = request.getParameter("distance");
//        logger.info("km " + km);

//        Distance distance = userService.findDistanceByCities(downloadcity, unloadcity);
//        logger.info("Client get distance " + distance);
////
//        request.setAttribute("km", distance.getDistance());

//        if (uploadDate!=null && weight!=null && volume!=null) {
//            int weightInt = Integer.parseInt(weight);
//            int volumeInt = Integer.parseInt(volume);
////            BigDecimal decimalSum = BigDecimal.valueOf(Double.parseDouble(sum));
////            logger.info("decimalSum" + decimalSum);
////            int kmInt = Integer.parseInt(km);
//
//             int sumInt = (distance.getDistance()*(weightInt+volumeInt))/4;
//            logger.info("SUM " + sumInt);
//
//            request.setAttribute("sum", sumInt);
//        }
        return "WEB-INF/client/client.jsp";

    }


}
