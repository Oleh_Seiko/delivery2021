package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Distance;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class CalculateResult implements Command {
    private static final Logger logger = LogManager.getLogger(CalculateResult.class);

    public UserService userService;

    public CalculateResult(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");
        final String cityStart = request.getParameter("city");
        request.setAttribute("city", cityStart);
        final String cityFinish = request.getParameter("city2");
        request.setAttribute("city2", cityFinish);
        String weight = request.getParameter("weight");
        request.setAttribute("weight", weight);
        String volume = request.getParameter("volume");
        request.setAttribute("volume", volume);

        Distance distance = userService.findDistanceByCities(cityStart, cityFinish);
        logger.info("km", distance.getKm());

        request.setAttribute("km", distance.getKm());

        int weightInt = Integer.parseInt(weight);
        int volumeInt = Integer.parseInt(volume);

        int sumInt = (distance.getKm()*(weightInt+volumeInt))/4;
        logger.info("SUM " + sumInt);

        request.setAttribute("sum", sumInt);

        return "/calculate-result.jsp";
    }
}
