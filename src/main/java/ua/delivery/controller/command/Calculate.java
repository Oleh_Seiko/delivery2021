package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Distance;
import ua.delivery.model.entity.enums.City;
import ua.delivery.model.service.UserService;
import javax.servlet.http.HttpServletRequest;

public class Calculate implements Command {

    private static final Logger logger = LogManager.getLogger(Calculate.class);

    private UserService userService;

    public Calculate(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {

        City[] cities = City.values();
        request.setAttribute("cities", cities);

        String cityStart = request.getParameter("city");
        String cityFinish = request.getParameter("city2");
        String weight = request.getParameter("weight");
        String volume = request.getParameter("volume");

        Distance distance= userService.findDistanceByCities(cityStart, cityFinish);
        logger.info("Calculation get distance " + distance);

        request.setAttribute("distance", distance.getKm());
        logger.info("distance " + distance.getKm());

        if (weight!= null && volume!= null) {

            int weightInt = Integer.parseInt(weight);
            int volumeInt = Integer.parseInt(volume);

            int sum = (distance.getKm()*(weightInt+volumeInt))/4;
            logger.info("SUM " + sum);

            request.setAttribute("sum", sum);
        }
        return "calculate.jsp";
    }
}
