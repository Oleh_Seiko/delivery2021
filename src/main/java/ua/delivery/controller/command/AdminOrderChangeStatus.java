package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Status;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdminOrderChangeStatus implements Command {
    private static final Logger logger = LogManager.getLogger(AdminOrderChangeStatus.class);
    private UserService userService;

    public AdminOrderChangeStatus(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");
        final String status = request.getParameter("status");
        System.out.println(status);
        final String orderId = request.getParameter("order-id");

        final int orderIdInt = Integer.parseInt(orderId);
        System.out.println("OrderIdINT " + orderIdInt);

        userService.changeStatus(orderIdInt, Status.valueOf(status));

        final List<Order> orderList = userService.findAllOrders();
        request.setAttribute("orderList", orderList);



        return "redirect:/admin/orders";
    }
}
