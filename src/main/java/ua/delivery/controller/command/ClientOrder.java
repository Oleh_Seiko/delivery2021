package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class ClientOrder implements Command {
    private static final Logger logger = LogManager.getLogger(ClientOrder.class);
    private UserService userService;

    public ClientOrder(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("method execute()");

        final User user = (User) request.getSession().getAttribute("user");
        final List<Order> orders = userService.findByOrderUserId(user.getId());
        System.out.println(orders);
        request.setAttribute("orders", orders);
//        final String[] ordersValues = request.getParameterValues("orders");
//        request.setAttribute("ordersValues", ordersValues);


        return "/WEB-INF/client/client-order.jsp";
    }
}
