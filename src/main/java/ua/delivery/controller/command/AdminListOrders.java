package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Status;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdminListOrders implements Command {
    private static final Logger logger = LogManager.getLogger(AdminListOrders.class);
    private UserService userService;

    public AdminListOrders(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created, execute()");
        Status[] statuses = Status.values();
        request.setAttribute("statuses", statuses);
//        final User user = (User) request.getSession().getAttribute("user");
        final List<Order> allOrders = userService.findAllOrders();
        request.setAttribute("allOrders", allOrders);

        return "/WEB-INF/admin/admin-listorders.jsp";
    }
}
