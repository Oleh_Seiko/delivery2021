package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.User;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class AdminListClient implements Command {
    private static final Logger logger = LogManager.getLogger(AdminListClient.class);
    private UserService userService;

    public AdminListClient(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("execute(), created");
//        final User user = (User) request.getSession().getAttribute("user");
        final List<User> allUsers = userService.findAllUsers();

        System.out.println("allUsers " + allUsers);
        request.setAttribute("allUsers", allUsers);

        return "/WEB-INF/admin/admin-listclient.jsp";
    }
}
