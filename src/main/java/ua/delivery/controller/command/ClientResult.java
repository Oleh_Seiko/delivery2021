package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Distance;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.City;
import ua.delivery.model.entity.enums.Status;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

public class ClientResult implements Command {
    private static final Logger logger = LogManager.getLogger(ClientResult.class);

    private UserService userService;

    public ClientResult(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created, execute()");

        City[] cities = City.values();
        request.setAttribute("cities", cities);
        Status[] statuses = Status.values();
        request.setAttribute("statuses", statuses);

        String dateStart = request.getParameter("date");
        request.setAttribute("date", dateStart);
        String cityStart = request.getParameter("city");
        request.setAttribute("city", cityStart);
        String cityFinish = request.getParameter("city2");
        request.setAttribute("city2", cityFinish);
        String weight = request.getParameter("weight");
        request.setAttribute("weight", weight);
        String volume = request.getParameter("volume");
        request.setAttribute("volume", volume);
        String status = request.getParameter("status");
        request.setAttribute("status", status);

        Distance distance = userService.findDistanceByCities(cityStart, cityFinish);
        logger.info("km", distance.getKm());

        request.setAttribute("km", distance.getKm());

        int weightInt = Integer.parseInt(weight);
        int volumeInt = Integer.parseInt(volume);

        int sumInt = (distance.getKm()*(weightInt+volumeInt))/4;
           logger.info("SUM " + sumInt);

            request.setAttribute("sum", sumInt);

            if (dateStart==null || dateStart.equals("") || cityStart==null || cityStart.equals("")
            || cityFinish==null || cityFinish.equals("") || weight.equals("")
            || volume.equals("")) {
                return "redirect:/WEB-INF/client/client.jsp";
            } else {

                User user = (User) request.getSession().getAttribute("user");

                Order order = new Order();
                order.setUser(user);
                order.setDateStart(Date.valueOf(dateStart));
                order.setCityStart(cityStart);
                order.setCityFinish(cityFinish);
                order.setWeight(Integer.parseInt(weight));
                order.setVolume(Integer.parseInt(volume));
                order.setKm(distance.getKm());
                order.setSum(sumInt);
                order.setStatus(Status.IN_PROCESSING);

                logger.info("ClientResult - orderUser " + order);
                userService.createOrder(order);
            }

        return "/WEB-INF/client/client-result.jsp";
    }
}
