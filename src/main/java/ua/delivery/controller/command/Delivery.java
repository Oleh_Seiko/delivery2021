package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;

import javax.servlet.http.HttpServletRequest;

public class Delivery implements Command {
    private static final Logger logger = LogManager.getLogger(Delivery.class);
    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");


        return "redirect:/login.jsp";
    }
}
