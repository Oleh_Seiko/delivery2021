package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.enums.Status;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdminOrderDelete implements Command {
    private static final Logger logger = LogManager.getLogger(AdminOrderDelete.class);
    private UserService userService;

    public AdminOrderDelete(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");


        return "redirect:/admin/orders";
    }
}
