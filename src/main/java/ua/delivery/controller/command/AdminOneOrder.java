package ua.delivery.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.delivery.controller.Command;
import ua.delivery.model.entity.Order;
import ua.delivery.model.entity.User;
import ua.delivery.model.entity.enums.Status;
import ua.delivery.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdminOneOrder implements Command {
    private static final Logger logger = LogManager.getLogger(AdminOneOrder.class);
    private UserService userService;

    public AdminOneOrder(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("created execute()");

        Status[] statuses = Status.values();
        request.setAttribute("statuses", statuses);

        String orderId = request.getParameter("oneOrder");
        int orderIdInt = Integer.parseInt(orderId);

        System.out.println("orderId " + orderId);

        final Order order = userService.findOrderById(orderIdInt);
        System.out.println("found order " + order);

        final int userId= order.getUser().getId();
        final User user = userService.findUserById(userId);
        order.setUser(user);


        request.setAttribute("order", order);

        return "/WEB-INF/admin/admin-one_order.jsp";
    }
}
