CREATE TABLE user
(
    id        INT PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR(32) NOT NULL,
    lastname  VARCHAR(32) NOT NULL,
    username  VARCHAR(32) NOT NULL UNIQUE,
    password  VARCHAR(32) NOT NULL,
    role      VARCHAR(32)
);

CREATE TABLE orders
(
    id         INT AUTO_INCREMENT,
    userId     INT         not null,
    dateStart  DATE        not null,
    cityStart  VARCHAR(32) not null,
    cityFinish VARCHAR(32) not null,
    weight     INT         not null,
    volume     INT         not null,
    km         INT         not null,
    sum        INT         not null,
    status     VARCHAR(20) null,
    constraint orders_pk
        primary key (id)
);

CREATE TABLE distance
(
    downloadcity VARCHAR(32) NOT NULL,
    unloadcity   VARCHAR(32) NOT NULL,
    distance     INT         NOT NULL
);
